
CONTROLLER = $(wildcard src/controller/*.java)
DAVES = $(wildcard src/daves/*.java)
INITIALIZE = $(wildcard src/initialize/*.java)
MODEL = $(wildcard src/model/*.java)
VIEW = $(wildcard src/view/*.java)


CONTROLLERCLASS = $(wildcard src/controller/*.class)
DAVESCLASS = $(wildcard src/daves/*.class)
INITIALIZECLASS = $(wildcard src/initialize/*.class)
MODELCLASS = $(wildcard src/model/*.class)
VIEWCLASS = $(wildcard src/view/*.class)



compile: 
	@javac $(CONTROLLER)
	@javac $(DAVES)
	@javac $(INITIALIZE)
	@javac $(MODEL)
	@javac $(VIEW)
	@echo compile complete

run:
	java src/controller/Big

clean:
	@rm -f $(CONTROLLERCLASS)
	@rm -f $(DAVESCLASS)
	@rm -f $(INITIALIZECLASS)
	@rm -f $(MODELCLASS)
	@rm -f $(VIEWCLASS)