package src.controller;

public interface CommObserver
{
	void update(String message);
}