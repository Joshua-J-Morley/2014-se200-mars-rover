package src.controller;

import src.model.*;

import java.util.*;
import java.io.*;

public class TaskFactory
{
	public TaskOption makeTask(String taskName, String list)
	{
		TaskOption task = null;
		String[] str = taskName.split(":");

		if(taskName.contains("Drive"))
		{
			task = new DriveTask(Double.parseDouble(str[1]));
			//System.out.print("adding drive with "+ str[1] + "\n");
		}
		else if (taskName.contains("Turn"))
		{
			task = new TurnTask(Double.parseDouble(str[1]));
			//System.out.print("adding Turn with "+ str[1] + "\n");
		}
		else if (taskName.contains("SampleSoil"))
		{
			task = new AnalyseSoilTask();
			//System.out.print("adding Analyse Soil \n");
		}
		else if (taskName.contains("TakePhoto"))
		{
			task = new TakePhotoTask();
			//System.out.print("adding Take Photo \n");
		}
		else if (taskName.contains("ExecuteList"))
		{
			task = new ExecuteTaskListTask(Integer.parseInt(str[1]), list);
			//System.out.print("ExecuteList for some reason \n");
		}
		else
		{
			//System.out.print("Incorrect message format \n");
		}

		//System.out.print("makeTask Complete\n\n");
		return task;
	}

}




/*Map<String, TaskOption> taskType = new HashMap<String, TaskOption>();
TaskOption task = null;
String[] str = taskName.split(":");

taskType.put("Drive", new DriveTask(Double.parseDouble(str[1])));
taskType.put("Turn", new TurnTask(Double.parseDouble(str[1])));
taskType.put("SampleSoil", new AnalyseSoilTask());
taskType.put("TakePhoto", new TakePhotoTask());
taskType.put("ExecuteList", new ExecuteTaskListTask(Integer.parseInt(str[1])));

task = taskType.get(taskName);

if(task == null)
{
	System.out.print("you fucked up sonnnnn");
}

return task;
*/

