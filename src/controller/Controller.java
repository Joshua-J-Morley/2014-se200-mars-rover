package src.controller;

import src.model.*;
import src.view.*;

import java.util.*;
import java.io.*;

public class Controller implements CommObserver
{
	private Map<Integer, ListOfTasks> operations = new HashMap<Integer, ListOfTasks>();
	private SubjectInterface commSub;

	public Controller(SubjectInterface inCommSub)
	{
		commSub = inCommSub;
		commSub.registerObserver(this);
	}

	public void addOperation(int listID, ListOfTasks list)
	{
		operations.put(listID, list);
	}

	public void update(String message)
	{
		TaskFactory factory = new TaskFactory();
		String subList;
		ListOfTasks list;

		String[] divMessage = message.split(";");
		int listID = Integer.parseInt(divMessage[0]);

		if(operations.get(listID) == null)
		{
			list = new ListOfTasks();
			list.addListID(listID);
			list.addTaskListString(message);

			for(int ii = 1; ii < divMessage.length; ii++)
			{
				if(divMessage[ii].contains("ExecuteList"))
				{
					String[] subStr = divMessage[ii].split(":");
					subList = (operations.get(Integer.parseInt(subStr[1]))).getTaskListString();
				}
				else
				{
					subList = null;
				}
				TaskOption task = factory.makeTask(divMessage[ii], subList);
				//System.out.print("Finished adding" + divMessage[ii] + "\n");
				list.addTask(task);
				
			}

			addOperation(list.getListID(), list);
			list.executeList();
			System.out.print("completed executing task list: " + list.getListID() + "\n\n");
		}
		else
		{
			list = operations.get(listID);
			list.executeList();
		}
	}
	
}