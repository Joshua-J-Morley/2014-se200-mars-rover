package src.controller;

import src.model.*;
import src.view.*;

import java.util.*;

public class ListOfTasks
{
	private List<TaskOption> listOfTasks = new ArrayList<TaskOption>();
	private String taskListString;
	private int listID;

	public ListOfTasks() {}

	public void addListID(int id)
	{
		listID = id;
	}

	public void addTask(TaskOption task)
	{
		listOfTasks.add(task);
	}

	public void addTaskListString(String inMessage)
	{
		taskListString = inMessage;
	}

	public int getListID()
	{
		return listID;
	}

	public String getTaskListString()
	{
		return taskListString;
	}

	public void executeList()
	{
		SubjectInterface comm;

		iterator:
		for(TaskOption curr : listOfTasks)
		{
			String str;
			str = curr.doTask();
			if(str.equals("Failed"))
			{
				comm.sendResults("Drive or Turn Failed, Stopping task list execution");
				break iterator;
			}
			else
			{
				comm.sendResults(str);
			}
		}
	}
}