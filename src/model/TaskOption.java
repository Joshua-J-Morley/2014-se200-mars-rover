package src.model;

public interface TaskOption
{
	String doTask();
}