package src.model;

import src.daves.*;

public class DriveTask extends Driver implements TaskOption
{
	private double distance;
	private int taskisComplete;
	private String taskisFinished;

	public DriveTask(double inParam)
	{
		distance = inParam;
		taskisComplete = 0;
		taskisFinished = new String();
	}

	public String doTask()
	{
		String retStr = new String();
		drive(distance);

		while(taskisComplete != 1)
		{
			retStr = finished();
		}

		return retStr;
		/*return "Success";*/
	}

	protected void moveFinished()
	{
		taskisFinished = new String("Success");
		taskComplete(1);
	}

	protected void mechanicalError()
	{
		taskisFinished = new String("Failed");
		taskComplete(1);
	}

	public String finished()
	{
		return taskisFinished;
	}

	public void taskComplete(int flag)
	{
		taskisComplete = flag;
	}
}