package src.model;

import src.daves.*;

public class TakePhotoTask extends Camera implements TaskOption
{
	private int taskisComplete;
	private String taskisFinished;

	public TakePhotoTask() {}

	public String doTask()
	{
		String photoResults = new String();
		takePhoto();

		while(taskisComplete != 1)
		{
			photoResults = finished();
		}

		return photoResults;
		/*return "photoResults";*/
	}

	protected void photoReady(char[] photoData)
	{
		taskisFinished = new String(photoData);
		taskComplete(1);
	}

	public String finished()
	{
		return taskisFinished;
	}

	public void taskComplete(int flag)
	{
		taskisComplete = flag;
	}

}