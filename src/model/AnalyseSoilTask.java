package src.model;

import src.daves.*;

public class AnalyseSoilTask extends SoilAnalyser implements TaskOption
{
	private int taskisComplete;
	private String taskisFinished;

	public AnalyseSoilTask() {}

	public String doTask()
	{
		String analysisResults = new String();
		analyse();
		
		while(taskisComplete != 1)
		{
			analysisResults = finished();
		}

		return analysisResults;
		/*return "analysisResults";*/
	}

	protected void analysisReady(String analysis)
	{
		taskisFinished = analysis;
		taskComplete(1);
	}

	public String finished()
	{
		return taskisFinished;
	}

	public void taskComplete(int flag)
	{
		taskisComplete = flag;
	}
}