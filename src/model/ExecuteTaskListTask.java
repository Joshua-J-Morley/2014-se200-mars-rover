package src.model;

import src.controller.*;
import src.daves.*;

public class ExecuteTaskListTask implements TaskOption
{
	private int taskListID;
	private String list;

	public ExecuteTaskListTask(int inTaskListID, String inList)
	{
		taskListID = inTaskListID;
		list = inList;
	}

	public String doTask()
	{
		CommObserver comm;
		comm.update(list);

		return "Success";
	}

}