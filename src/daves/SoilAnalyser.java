package src.daves;


public abstract class SoilAnalyser 
{
	public SoilAnalyser() {}

	public void analyse() 
	{
		System.out.print("sampling soil \n");
	}	//SampleSoil

	protected abstract void analysisReady(String analysis);
}