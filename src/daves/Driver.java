package src.daves;


public abstract class Driver 
{
	public Driver() {}

	public void drive(double distance) 
	{
		System.out.print("driving " + distance + "metres\n");
	} 	//Drive <M> where -100m <= M <= 100 
	
	public void turn(double angle) 
	{
		System.out.print("turning" + angle + "degrees\n");
	} 		//Turn <E>	where -180 <= E <= 180

	protected abstract void moveFinished();

	protected abstract void mechanicalError();
}