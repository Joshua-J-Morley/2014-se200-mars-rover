package src.view;

import src.controller.*;

public class CommSub extends Comm implements SubjectInterface
{
	private List<CommObserver> observers;

	public CommSub() 
	{
		observers = new ArrayList<CommObserver>();
	}

	protected void receive(String message)
	{
		notifyObservers(message);
	}

	public void registerObserver(CommObserver o)
	{
		observers.add(o);
	}

	public void removeObserver(CommObserver o)
	{
		int ii = observers.indexOf(o);
		if(ii >= 0)
		{
			observers.remove(ii);
		}
	}

	public void notifyObservers(String message)
	{
		for (int ii = 0; ii < observers.size(); ii++) 
		{
			CommObserver observer = (CommObserver)observers.get(ii);
			observer.update(message);	
		}
	}

	public void sendResults(String message)
	{
		send(message);
	}

}