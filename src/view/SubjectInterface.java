package src.view;

import src.controller.*;

public interface SubjectInterface
{
	public void registerObserver(CommObserver o);
	public void removeObserver(CommObserver o);
	public void notifyObservers(String message);
	public void sendResults(String message);
}