package src.initialize;

import src.model.*;
import src.view.*;
import src.daves.*;

public class TestInitializaer
{
	private static Scanner input = new Scanner(System.in);

	public static void main(String [] args)
	{
		new TestInitializaer().run();
	}

	public void run()
	{
		Initialize init = new Initialize();
		init.initializeRover();

		init.testRover();
	}
}